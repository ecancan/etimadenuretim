package com.ecancan.etimadenuretim.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.Datum;
import com.ecancan.etimadenuretim.ProductDetailActivity;
import com.ecancan.etimadenuretim.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductCardHolder>{
    private Context mContext;
    private List<Datum>  dataList;

    public ProductAdapter(Context mContext, List<Datum> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public ProductCardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_productlist,viewGroup,false);
        return new ProductCardHolder(view);
    }

    public class ProductCardHolder extends RecyclerView.ViewHolder{
        private TextView productCode,facilityAndShift,weight,package_palette,date;
        private CardView productCard;
        public ProductCardHolder(@NonNull View itemView) {
            super(itemView);
            productCard = itemView.findViewById(R.id.product_card);
            productCode = itemView.findViewById(R.id.satinAlmaBelgesiView);
            facilityAndShift = itemView.findViewById(R.id.teslimat_no);
            weight = itemView.findViewById(R.id.malzeme_kisa_aciklama);
            package_palette = itemView.findViewById(R.id.package_palette);
            date = itemView.findViewById(R.id.date);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCardHolder productCardHolder, int i) {
        final Datum datum = dataList.get(i);
        productCardHolder.productCode.setText(datum.getOrders());
        productCardHolder.facilityAndShift.setText("Tesis "+datum.getFacility()+" - V"+datum.getShift());
        productCardHolder.weight.setText(datum.getWeight()+ " " + datum.getWeighttype());
        productCardHolder.package_palette.setText(datum.getPackagePalette() + " ETK");
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(datum.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        productCardHolder.date.setText("Tarih : "+new SimpleDateFormat("dd.MM.yyyy").format(date));
        productCardHolder.productCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.putExtra("productDetail", datum);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
