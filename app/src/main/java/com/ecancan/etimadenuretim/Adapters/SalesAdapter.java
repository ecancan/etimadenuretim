package com.ecancan.etimadenuretim.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.SalesDatum;
import com.ecancan.etimadenuretim.R;
import com.ecancan.etimadenuretim.SalesDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.SalesCardHolder>{
    private Context mContext;
    private List<SalesDatum>  dataList;

    public SalesAdapter(Context mContext, List<SalesDatum> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public SalesCardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_sales,viewGroup,false);
        return new SalesCardHolder(view);
    }

    public class SalesCardHolder extends RecyclerView.ViewHolder{
        private TextView sapCode,yuklemeLimani,parti,date;
        private CardView salesCard;
        public SalesCardHolder(@NonNull View itemView) {
            super(itemView);
            salesCard = itemView.findViewById(R.id.sales_card);
            sapCode = itemView.findViewById(R.id.satinAlmaBelgesiView);
            yuklemeLimani = itemView.findViewById(R.id.teslimat_no);
            parti = itemView.findViewById(R.id.malzeme_kisa_aciklama);
            date = itemView.findViewById(R.id.cekmeTarihi);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull SalesCardHolder salesCardHolder, int i) {
        final SalesDatum datum = dataList.get(i);
        salesCardHolder.sapCode.setText(datum.getTeslimatNo());
        salesCardHolder.yuklemeLimani.setText("Yükleme Limanı : " + datum.getYuklemeLimani());
        salesCardHolder.parti.setText("Parti : " + datum.getParti());
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(datum.getCekmeTarihi());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        salesCardHolder.date.setText("Tarih : "+new SimpleDateFormat("dd.MM.yyyy").format(date));


        salesCardHolder.salesCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SalesDetailActivity.class);
                intent.putExtra("salesDetail", datum);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
