package com.ecancan.etimadenuretim.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.ShippingDatum;
import com.ecancan.etimadenuretim.R;
import com.ecancan.etimadenuretim.ShippingDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ShippingAdapter extends RecyclerView.Adapter<ShippingAdapter.ShippingCardHolder>{
    private Context mContext;
    private List<ShippingDatum> dataList;

    public ShippingAdapter(Context mContext, List<ShippingDatum> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }


    @NonNull
    @Override
    public ShippingCardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_shipping,viewGroup,false);
        return new ShippingCardHolder(view);
    }

    public class ShippingCardHolder extends RecyclerView.ViewHolder{
        private TextView satinAlmaBelgesi,teslimatNo,malzemeAciklamasi,date;
        private CardView shippingCard;
        public ShippingCardHolder(@NonNull View itemView) {
            super(itemView);
            shippingCard = itemView.findViewById(R.id.shipping_card);
            satinAlmaBelgesi = itemView.findViewById(R.id.satinAlmaBelgesiView);
            teslimatNo = itemView.findViewById(R.id.teslimat_no);
            malzemeAciklamasi = itemView.findViewById(R.id.malzeme_kisa_aciklama);
            date = itemView.findViewById(R.id.islemTarihi);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull ShippingCardHolder shippingCardHolder, int i) {
        final ShippingDatum datum = dataList.get(i);

        shippingCardHolder.satinAlmaBelgesi.setText(datum.getSatinAlmaBelgesi());
        shippingCardHolder.teslimatNo.setText("Teslimat : " + datum.getTeslimatNo());
        shippingCardHolder.malzemeAciklamasi.setText(datum.getMalzemeKisaMetni());
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(datum.getIslemTarihi());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        shippingCardHolder.date.setText("Tarih : "+new SimpleDateFormat("dd.MM.yyyy").format(date));




        shippingCardHolder.shippingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ShippingDetailActivity.class);
                intent.putExtra("shippingDetail", datum);
                mContext.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
