package com.ecancan.etimadenuretim;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Adapters.SalesAdapter;
import com.ecancan.etimadenuretim.Interfaces.IProductDataAccessObject;
import com.ecancan.etimadenuretim.Modules.ApiUtils;
import com.ecancan.etimadenuretim.Modules.SalesDataList;
import com.ecancan.etimadenuretim.Modules.SalesDatum;
import com.ecancan.etimadenuretim.Modules.SalesSearchFilter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private IProductDataAccessObject salesDIF;
    private SalesAdapter adapter;
    private RecyclerView rv;
    private int totalSalesCount = 0;
    private ImageView searchButton;
    private EditText teslimatNo,plakaNo,partiNo;
    private Button searchDialogButton,searchDialogButtonReset;
    private TextView dateSelect;
    private TextView dateView;
    private TextView totalSales,totalSalesWeightView;
    private int totalSalesWeightCount = 0;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private SalesSearchFilter salesSearchFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
        searchButton = findViewById(R.id.searchButton);
        dateView = findViewById(R.id.dateView);
        totalSales = findViewById(R.id.totalVehicle);
        totalSalesWeightView = findViewById(R.id.totalWeightSales);
        toolbar = findViewById(R.id.toolbar);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");

        sp = getSharedPreferences("searchFilterData",MODE_PRIVATE);
        editor = sp.edit();

        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Satış Listesi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        salesDIF = ApiUtils.getIProductDataAccessObject();
        allSalesData(FirebaseAuth.getInstance().getCurrentUser().getUid(),"","","","");



        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(SalesActivity.this);
                dialog.setContentView(R.layout.sales_search_dialog);
                dialog.setTitle("Satış Filtreleme");
                teslimatNo = dialog.findViewById(R.id.satin_alma_belgesi);
                plakaNo = dialog.findViewById(R.id.teslimat_no);
                partiNo = dialog.findViewById(R.id.bir_kantar_no);

                dateSelect = dialog.findViewById(R.id.dateSelect);
                searchDialogButton = dialog.findViewById(R.id.searchFilterButton);
                searchDialogButtonReset = dialog.findViewById(R.id.searchFilterButtonReset);

                try{
                    sp = getSharedPreferences("searchFilterData",MODE_PRIVATE);
                    editor = sp.edit();
                    Gson gson = new Gson();
                    String json = sp.getString("searchFilterData", "");
                    salesSearchFilter = gson.fromJson(json, SalesSearchFilter.class);
                    dateSelect.setText(salesSearchFilter.getCekmeTarihi());
                    teslimatNo.setText(salesSearchFilter.getTeslimatNo());
                    plakaNo.setText(salesSearchFilter.getBookingNo());
                    partiNo.setText(salesSearchFilter.getPartiNo());
                }catch (Exception e){

                }

                dateSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePicker = new DatePickerDialog(SalesActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        month = month +1;
                                        String dateString = year +"-"+month+"-"+dayOfMonth;
                                        dateView.setText(dayOfMonth+"."+month+"."+year);
                                        Date date = null;
                                        try {
                                            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        dateSelect.setText(new SimpleDateFormat("yyyy-MM-dd").format(date));

                                    }
                                }, year, month, day);

                        datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", datePicker);
                        datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker);
                        datePicker.show();
                    }
                });
                searchDialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String getFilterDate = dateSelect.getText().toString();
                        String teslimatNoFilter = teslimatNo.getText().toString();
                        String plakaNoFilter = plakaNo.getText().toString();
                        String partiNoFilter = partiNo.getText().toString();

                        salesSearchFilter = new SalesSearchFilter(teslimatNoFilter,plakaNoFilter,partiNoFilter,getFilterDate);
                        Gson gson = new Gson();
                        String json = gson.toJson(salesSearchFilter);
                        editor.putString("searchFilterData", json);
                        editor.commit();

                        getFilterDate = getFilterDate.replace("Tarih Seçiniz","");
                        Log.e("date",getFilterDate);
                        Log.e("teslimat",teslimatNoFilter);
                        Log.e("plaka",plakaNoFilter);
                        Log.e("parti",partiNoFilter);
                        if(getFilterDate == ""){
                            dateView.setText("Tüm Zamanlar");
                        }


                        allSalesData(FirebaseAuth.getInstance().getCurrentUser().getUid(),teslimatNoFilter,plakaNoFilter,partiNoFilter,getFilterDate);
                        dialog.dismiss();
                    }
                });
                searchDialogButtonReset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateSelect.setText("Tarih Seçiniz");
                        teslimatNo.setText("");
                        plakaNo.setText("");
                        partiNo.setText("");
                        sp = getSharedPreferences("searchFilterData",MODE_PRIVATE);
                        editor = sp.edit();
                        sp.edit().remove("searchFilterData").commit();
                    }
                });

                dialog.show();
            }
        });

    }

    public void allSalesData(String userid,String teslimat_no,String plaka,String parti,String cekme_tarihi){
        final Dialog dialogwait = new Dialog(SalesActivity.this);
        dialogwait.setContentView(R.layout.wait_dialog);
        dialogwait.show();
        dialogwait.setCanceledOnTouchOutside(false);
        dialogwait.setCancelable(false);
        salesDIF.allSalesData(userid,teslimat_no,plaka,parti,cekme_tarihi).enqueue(new Callback<SalesDataList>() {
            @Override
            public void onResponse(Call<SalesDataList> call, Response<SalesDataList> response) {

                List<SalesDatum> dataList = response.body().getData();
                adapter = new SalesAdapter(SalesActivity.this,dataList);
                if(dataList != null){
                    totalSalesCount = 0;
                    totalSalesWeightCount = 0;
                    for(SalesDatum d: dataList){
                        totalSalesCount++;
                        totalSalesWeightCount += Integer.parseInt(d.getTeslimatMiktari());
                    }
                    totalSales.setText(Integer.toString(totalSalesCount));
                    totalSalesWeightView.setText(Integer.toString(totalSalesWeightCount) +" KG");
                    rv.setVisibility(View.VISIBLE);
                    rv.setAdapter(adapter);
                    dialogwait.dismiss();
                }else{
                    totalSales.setText("0");
                    totalSalesWeightView.setText("0 KG");
                    dialogwait.dismiss();
                    rv.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onFailure(Call<SalesDataList> call, Throwable t) {

            }
        });
    }

}
