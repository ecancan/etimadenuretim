package com.ecancan.etimadenuretim.Modules;

public class SalesSearchFilter {
    private String teslimatNo;
    private String bookingNo;
    private String partiNo;
    private String cekmeTarihi;

    public SalesSearchFilter() {
    }

    public SalesSearchFilter(String teslimatNo, String bookingNo, String partiNo, String cekmeTarihi) {
        this.teslimatNo = teslimatNo;
        this.bookingNo = bookingNo;
        this.partiNo = partiNo;
        this.cekmeTarihi = cekmeTarihi;
    }

    public String getTeslimatNo() {
        return teslimatNo;
    }

    public void setTeslimatNo(String teslimatNo) {
        this.teslimatNo = teslimatNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getPartiNo() {
        return partiNo;
    }

    public void setPartiNo(String partiNo) {
        this.partiNo = partiNo;
    }

    public String getCekmeTarihi() {
        return cekmeTarihi;
    }

    public void setCekmeTarihi(String cekmeTarihi) {
        this.cekmeTarihi = cekmeTarihi;
    }
}
