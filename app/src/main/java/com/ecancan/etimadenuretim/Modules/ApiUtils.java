package com.ecancan.etimadenuretim.Modules;

import com.ecancan.etimadenuretim.Interfaces.IProductDataAccessObject;

public class ApiUtils {
    public static final String BASE_URL = "http://etimadenuretim.ecancan.com/";
    public static IProductDataAccessObject getIProductDataAccessObject(){
        return RetrofitClient.getClient(BASE_URL).create(IProductDataAccessObject.class);
    }

}
