
package com.ecancan.etimadenuretim.Modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SalesDatum implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("teslimat_no")
    @Expose
    private String teslimatNo;
    @SerializedName("sefer_no")
    @Expose
    private String seferNo;
    @SerializedName("malzeme_kisa")
    @Expose
    private String malzemeKisa;
    @SerializedName("parti")
    @Expose
    private String parti;
    @SerializedName("teslimat_miktari")
    @Expose
    private String teslimatMiktari;
    @SerializedName("teslim_eden_yer")
    @Expose
    private String teslimEdenYer;
    @SerializedName("depo_yeri")
    @Expose
    private String depoYeri;
    @SerializedName("konteyner_no")
    @Expose
    private String konteynerNo;
    @SerializedName("booking_no")
    @Expose
    private String bookingNo;
    @SerializedName("konteyner_tr")
    @Expose
    private String konteynerTr;
    @SerializedName("ambalaj_aciklama")
    @Expose
    private String ambalajAciklama;
    @SerializedName("palet_tip_aciklama")
    @Expose
    private String paletTipAciklama;
    @SerializedName("torba_tanimi")
    @Expose
    private String torbaTanimi;
    @SerializedName("palet_t_aciklama")
    @Expose
    private String paletTAciklama;
    @SerializedName("torbalama_aciklama")
    @Expose
    private String torbalamaAciklama;
    @SerializedName("shrink")
    @Expose
    private String shrink;
    @SerializedName("cift_kulak")
    @Expose
    private String ciftKulak;
    @SerializedName("alttan_bosaltma")
    @Expose
    private String alttanBosaltma;
    @SerializedName("buffle")
    @Expose
    private String buffle;
    @SerializedName("palet_sayisi")
    @Expose
    private String paletSayisi;
    @SerializedName("tane_boyutu")
    @Expose
    private String taneBoyutu;
    @SerializedName("kilit_no")
    @Expose
    private String kilitNo;
    @SerializedName("bir_kantar_ilk_tartim")
    @Expose
    private String birKantarIlkTartim;
    @SerializedName("bir_kantar_ikinci_tartim")
    @Expose
    private String birKantarIkinciTartim;
    @SerializedName("bir_kantar_islem_tarihi")
    @Expose
    private String birKantarIslemTarihi;
    @SerializedName("bir_kantar_islem_saati")
    @Expose
    private String birKantarIslemSaati;
    @SerializedName("plaka")
    @Expose
    private String plaka;
    @SerializedName("cekme_tarihi")
    @Expose
    private String cekmeTarihi;
    @SerializedName("yukleme_tarihi")
    @Expose
    private String yuklemeTarihi;
    @SerializedName("alici_adi")
    @Expose
    private String aliciAdi;
    @SerializedName("siparis_no")
    @Expose
    private String siparisNo;
    @SerializedName("net")
    @Expose
    private String net;
    @SerializedName("yukleme_limani")
    @Expose
    private String yuklemeLimani;
    @SerializedName("bosaltma_limani")
    @Expose
    private String bosaltmaLimani;
    @SerializedName("zayi_edilen_miktar")
    @Expose
    private String zayiEdilenMiktar;
    @SerializedName("iptal_nedeni_tanimi")
    @Expose
    private String iptalNedeniTanimi;
    @SerializedName("adi_soyadi")
    @Expose
    private String adiSoyadi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeslimatNo() {
        return teslimatNo;
    }

    public void setTeslimatNo(String teslimatNo) {
        this.teslimatNo = teslimatNo;
    }

    public String getSeferNo() {
        return seferNo;
    }

    public void setSeferNo(String seferNo) {
        this.seferNo = seferNo;
    }

    public String getMalzemeKisa() {
        return malzemeKisa;
    }

    public void setMalzemeKisa(String malzemeKisa) {
        this.malzemeKisa = malzemeKisa;
    }

    public String getParti() {
        return parti;
    }

    public void setParti(String parti) {
        this.parti = parti;
    }

    public String getTeslimatMiktari() {
        return teslimatMiktari;
    }

    public void setTeslimatMiktari(String teslimatMiktari) {
        this.teslimatMiktari = teslimatMiktari;
    }

    public String getTeslimEdenYer() {
        return teslimEdenYer;
    }

    public void setTeslimEdenYer(String teslimEdenYer) {
        this.teslimEdenYer = teslimEdenYer;
    }

    public String getDepoYeri() {
        return depoYeri;
    }

    public void setDepoYeri(String depoYeri) {
        this.depoYeri = depoYeri;
    }

    public String getKonteynerNo() {
        return konteynerNo;
    }

    public void setKonteynerNo(String konteynerNo) {
        this.konteynerNo = konteynerNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getKonteynerTr() {
        return konteynerTr;
    }

    public void setKonteynerTr(String konteynerTr) {
        this.konteynerTr = konteynerTr;
    }

    public String getAmbalajAciklama() {
        return ambalajAciklama;
    }

    public void setAmbalajAciklama(String ambalajAciklama) {
        this.ambalajAciklama = ambalajAciklama;
    }

    public String getPaletTipAciklama() {
        return paletTipAciklama;
    }

    public void setPaletTipAciklama(String paletTipAciklama) {
        this.paletTipAciklama = paletTipAciklama;
    }

    public String getTorbaTanimi() {
        return torbaTanimi;
    }

    public void setTorbaTanimi(String torbaTanimi) {
        this.torbaTanimi = torbaTanimi;
    }

    public String getPaletTAciklama() {
        return paletTAciklama;
    }

    public void setPaletTAciklama(String paletTAciklama) {
        this.paletTAciklama = paletTAciklama;
    }

    public String getTorbalamaAciklama() {
        return torbalamaAciklama;
    }

    public void setTorbalamaAciklama(String torbalamaAciklama) {
        this.torbalamaAciklama = torbalamaAciklama;
    }

    public String getShrink() {
        return shrink;
    }

    public void setShrink(String shrink) {
        this.shrink = shrink;
    }

    public String getCiftKulak() {
        return ciftKulak;
    }

    public void setCiftKulak(String ciftKulak) {
        this.ciftKulak = ciftKulak;
    }

    public String getAlttanBosaltma() {
        return alttanBosaltma;
    }

    public void setAlttanBosaltma(String alttanBosaltma) {
        this.alttanBosaltma = alttanBosaltma;
    }

    public String getBuffle() {
        return buffle;
    }

    public void setBuffle(String buffle) {
        this.buffle = buffle;
    }

    public String getPaletSayisi() {
        return paletSayisi;
    }

    public void setPaletSayisi(String paletSayisi) {
        this.paletSayisi = paletSayisi;
    }

    public String getTaneBoyutu() {
        return taneBoyutu;
    }

    public void setTaneBoyutu(String taneBoyutu) {
        this.taneBoyutu = taneBoyutu;
    }

    public String getKilitNo() {
        return kilitNo;
    }

    public void setKilitNo(String kilitNo) {
        this.kilitNo = kilitNo;
    }

    public String getBirKantarIlkTartim() {
        return birKantarIlkTartim;
    }

    public void setBirKantarIlkTartim(String birKantarIlkTartim) {
        this.birKantarIlkTartim = birKantarIlkTartim;
    }

    public String getBirKantarIkinciTartim() {
        return birKantarIkinciTartim;
    }

    public void setBirKantarIkinciTartim(String birKantarIkinciTartim) {
        this.birKantarIkinciTartim = birKantarIkinciTartim;
    }

    public String getBirKantarIslemTarihi() {
        return birKantarIslemTarihi;
    }

    public void setBirKantarIslemTarihi(String birKantarIslemTarihi) {
        this.birKantarIslemTarihi = birKantarIslemTarihi;
    }

    public String getBirKantarIslemSaati() {
        return birKantarIslemSaati;
    }

    public void setBirKantarIslemSaati(String birKantarIslemSaati) {
        this.birKantarIslemSaati = birKantarIslemSaati;
    }

    public String getPlaka() {
        return plaka;
    }

    public void setPlaka(String plaka) {
        this.plaka = plaka;
    }

    public String getCekmeTarihi() {
        return cekmeTarihi;
    }

    public void setCekmeTarihi(String cekmeTarihi) {
        this.cekmeTarihi = cekmeTarihi;
    }

    public String getYuklemeTarihi() {
        return yuklemeTarihi;
    }

    public void setYuklemeTarihi(String yuklemeTarihi) {
        this.yuklemeTarihi = yuklemeTarihi;
    }

    public String getAliciAdi() {
        return aliciAdi;
    }

    public void setAliciAdi(String aliciAdi) {
        this.aliciAdi = aliciAdi;
    }

    public String getSiparisNo() {
        return siparisNo;
    }

    public void setSiparisNo(String siparisNo) {
        this.siparisNo = siparisNo;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getYuklemeLimani() {
        return yuklemeLimani;
    }

    public void setYuklemeLimani(String yuklemeLimani) {
        this.yuklemeLimani = yuklemeLimani;
    }

    public String getBosaltmaLimani() {
        return bosaltmaLimani;
    }

    public void setBosaltmaLimani(String bosaltmaLimani) {
        this.bosaltmaLimani = bosaltmaLimani;
    }

    public String getZayiEdilenMiktar() {
        return zayiEdilenMiktar;
    }

    public void setZayiEdilenMiktar(String zayiEdilenMiktar) {
        this.zayiEdilenMiktar = zayiEdilenMiktar;
    }

    public String getIptalNedeniTanimi() {
        return iptalNedeniTanimi;
    }

    public void setIptalNedeniTanimi(String iptalNedeniTanimi) {
        this.iptalNedeniTanimi = iptalNedeniTanimi;
    }

    public String getAdiSoyadi() {
        return adiSoyadi;
    }

    public void setAdiSoyadi(String adiSoyadi) {
        this.adiSoyadi = adiSoyadi;
    }

}
