
package com.ecancan.etimadenuretim.Modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("facility")
    @Expose
    private String facility;
    @SerializedName("shift")
    @Expose
    private String shift;
    @SerializedName("orders")
    @Expose
    private String orders;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("materialdesc")
    @Expose
    private String materialdesc;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("weighttype")
    @Expose
    private String weighttype;
    @SerializedName("package_palette")
    @Expose
    private String packagePalette;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("baggedShape")
    @Expose
    private String baggedShape;
    @SerializedName("bagType")
    @Expose
    private String bagType;
    @SerializedName("C")
    @Expose
    private String c;
    @SerializedName("A")
    @Expose
    private String a;
    @SerializedName("B")
    @Expose
    private String b;
    @SerializedName("C2")
    @Expose
    private String c2;
    @SerializedName("K")
    @Expose
    private String k;
    @SerializedName("palette")
    @Expose
    private String palette;
    @SerializedName("packageType")
    @Expose
    private String packageType;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterialdesc() {
        return materialdesc;
    }

    public void setMaterialdesc(String materialdesc) {
        this.materialdesc = materialdesc;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeighttype() {
        return weighttype;
    }

    public void setWeighttype(String weighttype) {
        this.weighttype = weighttype;
    }

    public String getPackagePalette() {
        return packagePalette;
    }

    public void setPackagePalette(String packagePalette) {
        this.packagePalette = packagePalette;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBaggedShape() {
        return baggedShape;
    }

    public void setBaggedShape(String baggedShape) {
        this.baggedShape = baggedShape;
    }

    public String getBagType() {
        return bagType;
    }

    public void setBagType(String bagType) {
        this.bagType = bagType;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        this.c2 = c2;
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getPalette() {
        return palette;
    }

    public void setPalette(String palette) {
        this.palette = palette;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
