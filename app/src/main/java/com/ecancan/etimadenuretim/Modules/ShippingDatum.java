
package com.ecancan.etimadenuretim.Modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShippingDatum implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("teslimat_no")
    @Expose
    private String teslimatNo;
    @SerializedName("sefer_no")
    @Expose
    private String seferNo;
    @SerializedName("satin_alma_belgesi")
    @Expose
    private String satinAlmaBelgesi;
    @SerializedName("malzeme_kisa_metni")
    @Expose
    private String malzemeKisaMetni;
    @SerializedName("parti")
    @Expose
    private String parti;
    @SerializedName("teslim_eden_uretim_adi")
    @Expose
    private String teslimEdenUretimAdi;
    @SerializedName("teslim_eden_depo_adi")
    @Expose
    private String teslimEdenDepoAdi;
    @SerializedName("tane_boyutu")
    @Expose
    private String taneBoyutu;
    @SerializedName("olculen_miktar")
    @Expose
    private String olculenMiktar;
    @SerializedName("ilk_tartim")
    @Expose
    private String ilkTartim;
    @SerializedName("ikinci_tartim")
    @Expose
    private String ikinciTartim;
    @SerializedName("islem_tarihi")
    @Expose
    private String islemTarihi;
    @SerializedName("islem_saati")
    @Expose
    private String islemSaati;
    @SerializedName("plaka")
    @Expose
    private String plaka;
    @SerializedName("teslim_alan_depo_yeri")
    @Expose
    private String teslimAlanDepoYeri;
    @SerializedName("birinci_kantar")
    @Expose
    private String birinciKantar;
    @SerializedName("ikinci_kantar")
    @Expose
    private String ikinciKantar;
    @SerializedName("sofor_adi_soyadi")
    @Expose
    private String soforAdiSoyadi;
    @SerializedName("malzeme")
    @Expose
    private String malzeme;
    @SerializedName("bir_kantar_no")
    @Expose
    private String birKantarNo;
    @SerializedName("iki_kantar_no")
    @Expose
    private String ikiKantarNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeslimatNo() {
        return teslimatNo;
    }

    public void setTeslimatNo(String teslimatNo) {
        this.teslimatNo = teslimatNo;
    }

    public String getSeferNo() {
        return seferNo;
    }

    public void setSeferNo(String seferNo) {
        this.seferNo = seferNo;
    }

    public String getSatinAlmaBelgesi() {
        return satinAlmaBelgesi;
    }

    public void setSatinAlmaBelgesi(String satinAlmaBelgesi) {
        this.satinAlmaBelgesi = satinAlmaBelgesi;
    }

    public String getMalzemeKisaMetni() {
        return malzemeKisaMetni;
    }

    public void setMalzemeKisaMetni(String malzemeKisaMetni) {
        this.malzemeKisaMetni = malzemeKisaMetni;
    }

    public String getParti() {
        return parti;
    }

    public void setParti(String parti) {
        this.parti = parti;
    }

    public String getTeslimEdenUretimAdi() {
        return teslimEdenUretimAdi;
    }

    public void setTeslimEdenUretimAdi(String teslimEdenUretimAdi) {
        this.teslimEdenUretimAdi = teslimEdenUretimAdi;
    }

    public String getTeslimEdenDepoAdi() {
        return teslimEdenDepoAdi;
    }

    public void setTeslimEdenDepoAdi(String teslimEdenDepoAdi) {
        this.teslimEdenDepoAdi = teslimEdenDepoAdi;
    }

    public String getTaneBoyutu() {
        return taneBoyutu;
    }

    public void setTaneBoyutu(String taneBoyutu) {
        this.taneBoyutu = taneBoyutu;
    }

    public String getOlculenMiktar() {
        return olculenMiktar;
    }

    public void setOlculenMiktar(String olculenMiktar) {
        this.olculenMiktar = olculenMiktar;
    }

    public String getIlkTartim() {
        return ilkTartim;
    }

    public void setIlkTartim(String ilkTartim) {
        this.ilkTartim = ilkTartim;
    }

    public String getIkinciTartim() {
        return ikinciTartim;
    }

    public void setIkinciTartim(String ikinciTartim) {
        this.ikinciTartim = ikinciTartim;
    }

    public String getIslemTarihi() {
        return islemTarihi;
    }

    public void setIslemTarihi(String islemTarihi) {
        this.islemTarihi = islemTarihi;
    }

    public String getIslemSaati() {
        return islemSaati;
    }

    public void setIslemSaati(String islemSaati) {
        this.islemSaati = islemSaati;
    }

    public String getPlaka() {
        return plaka;
    }

    public void setPlaka(String plaka) {
        this.plaka = plaka;
    }

    public String getTeslimAlanDepoYeri() {
        return teslimAlanDepoYeri;
    }

    public void setTeslimAlanDepoYeri(String teslimAlanDepoYeri) {
        this.teslimAlanDepoYeri = teslimAlanDepoYeri;
    }

    public String getBirinciKantar() {
        return birinciKantar;
    }

    public void setBirinciKantar(String birinciKantar) {
        this.birinciKantar = birinciKantar;
    }

    public String getIkinciKantar() {
        return ikinciKantar;
    }

    public void setIkinciKantar(String ikinciKantar) {
        this.ikinciKantar = ikinciKantar;
    }

    public String getSoforAdiSoyadi() {
        return soforAdiSoyadi;
    }

    public void setSoforAdiSoyadi(String soforAdiSoyadi) {
        this.soforAdiSoyadi = soforAdiSoyadi;
    }

    public String getMalzeme() {
        return malzeme;
    }

    public void setMalzeme(String malzeme) {
        this.malzeme = malzeme;
    }

    public String getBirKantarNo() {
        return birKantarNo;
    }

    public void setBirKantarNo(String birKantarNo) {
        this.birKantarNo = birKantarNo;
    }

    public String getIkiKantarNo() {
        return ikiKantarNo;
    }

    public void setIkiKantarNo(String ikiKantarNo) {
        this.ikiKantarNo = ikiKantarNo;
    }

}
