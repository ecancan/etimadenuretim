package com.ecancan.etimadenuretim.Modules;

public class ShippingSearchFilter {
    private String satin_alma_belgesi;
    private String malzeme;
    private String bir_kantar_no;
    private String iki_kantar_no;
    private String islem_tarihi;
    private String plaka;

    public ShippingSearchFilter() {
    }

    public ShippingSearchFilter(String satin_alma_belgesi, String malzeme, String bir_kantar_no, String iki_kantar_no, String islem_tarihi, String plaka) {
        this.satin_alma_belgesi = satin_alma_belgesi;
        this.malzeme = malzeme;
        this.bir_kantar_no = bir_kantar_no;
        this.iki_kantar_no = iki_kantar_no;
        this.islem_tarihi = islem_tarihi;
        this.plaka = plaka;
    }

    public String getSatin_alma_belgesi() {
        return satin_alma_belgesi;
    }

    public void setSatin_alma_belgesi(String satin_alma_belgesi) {
        this.satin_alma_belgesi = satin_alma_belgesi;
    }

    public String getMalzeme() {
        return malzeme;
    }

    public void setMalzeme(String malzeme) {
        this.malzeme = malzeme;
    }

    public String getBir_kantar_no() {
        return bir_kantar_no;
    }

    public void setBir_kantar_no(String bir_kantar_no) {
        this.bir_kantar_no = bir_kantar_no;
    }

    public String getIki_kantar_no() {
        return iki_kantar_no;
    }

    public void setIki_kantar_no(String iki_kantar_no) {
        this.iki_kantar_no = iki_kantar_no;
    }

    public String getIslem_tarihi() {
        return islem_tarihi;
    }

    public void setIslem_tarihi(String islem_tarihi) {
        this.islem_tarihi = islem_tarihi;
    }

    public String getPlaka() {
        return plaka;
    }

    public void setPlaka(String plaka) {
        this.plaka = plaka;
    }
}
