
package com.ecancan.etimadenuretim.Modules;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingDataList {

    @SerializedName("data")
    @Expose
    private List<ShippingDatum> data = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<ShippingDatum> getData() {
        return data;
    }

    public void setData(List<ShippingDatum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
