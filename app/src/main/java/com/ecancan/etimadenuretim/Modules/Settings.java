package com.ecancan.etimadenuretim.Modules;

public class Settings {
    private String userRegistering;

    public Settings() {
    }

    public Settings(String userRegistering) {
        this.userRegistering = userRegistering;
    }

    public String getUserRegistering() {
        return userRegistering;
    }

    public void setUserRegistering(String userRegistering) {
        this.userRegistering = userRegistering;
    }
}
