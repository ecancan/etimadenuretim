package com.ecancan.etimadenuretim.Modules;

import java.io.Serializable;

public class Users implements Serializable {
    private String userKey;
    private String userLevel;
    private String userMail;
    private String userFullName;
    private int superUser;
    private String userToken;

    public Users(String userKey, String userLevel, String userMail, String userFullName, int superUser, String userToken) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.userMail = userMail;
        this.userFullName = userFullName;
        this.superUser = superUser;
        this.userToken = userToken;
    }

    public Users(String userKey, String userLevel, String userMail, String userFullName, int superUser) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.userMail = userMail;
        this.userFullName = userFullName;
        this.superUser = superUser;
    }

    public Users() {
    }

    public Users(String userKey, String userLevel, String userMail,String userFullName) {
        this.userKey = userKey;
        this.userLevel = userLevel;
        this.userMail = userMail;
        this.userFullName = userFullName;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public int getSuperUser() {
        return superUser;
    }

    public void setSuperUser(int superUser) {
        this.superUser = superUser;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
