
package com.ecancan.etimadenuretim.Modules;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesDataList {

    @SerializedName("data")
    @Expose
    private List<SalesDatum> data = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<SalesDatum> getData() {
        return data;
    }

    public void setData(List<SalesDatum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
