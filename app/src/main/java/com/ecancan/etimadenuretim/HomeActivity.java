package com.ecancan.etimadenuretim;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenuretim.Adapters.ProductAdapter;
import com.ecancan.etimadenuretim.Interfaces.IProductDataAccessObject;
import com.ecancan.etimadenuretim.Modules.ApiUtils;
import com.ecancan.etimadenuretim.Modules.Datum;
import com.ecancan.etimadenuretim.Modules.ProductDataList;
import com.ecancan.etimadenuretim.Modules.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private IProductDataAccessObject productDIF;
    private ProductAdapter adapter;
    private RecyclerView rv;
    private int totalTag = 0;
    private TextView totalTagView;
    private ImageView searchButton;
    private Spinner facility;
    private Spinner shift;
    private Button searchDialogButton;
    private TextView dateSelect;
    private TextView dateView;
    private TextView totalWeight;
    private int totalWeightCount = 0;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private View navigationHeader;
    private TextView userFullNameText,userLevetText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        totalTagView = findViewById(R.id.totalVehicle);
        searchButton = findViewById(R.id.searchButton);
        dateView = findViewById(R.id.dateView);
        totalWeight = findViewById(R.id.totalweight);
        toolbar = findViewById(R.id.toolbar);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");


        navigationView = (NavigationView) findViewById(R.id.navigationView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Üretim Listesi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,0,0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().removeItem(R.id.action_product);
        navigationView.getMenu().findItem(R.id.action_settings).setVisible(false);

        navigationHeader = (View) navigationView.inflateHeaderView(R.layout.navigation_header);
        userFullNameText = (TextView) navigationHeader.findViewById(R.id.header_user_full_name);
        userLevetText = (TextView) navigationHeader.findViewById(R.id.header_user_level);



        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).getValue(Users.class);
                userLevetText.setText(users.getUserLevel());
                userFullNameText.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
                if(users.getUserLevel().equals("Yönetici")){
                    navigationView.getMenu().findItem(R.id.action_settings).setVisible(true);
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        productDIF = ApiUtils.getIProductDataAccessObject();
        allData(FirebaseAuth.getInstance().getCurrentUser().getUid(),"","","");



        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(HomeActivity.this);
                dialog.setContentView(R.layout.search_dialog);
                dialog.setTitle("Üretim Filtreleme");
                facility = dialog.findViewById(R.id.satin_alma_belgesi);
                shift = dialog.findViewById(R.id.teslimat_no);


                ArrayAdapter<String> productFacilityAtapter = new ArrayAdapter<String>(HomeActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.productFacility));
                productFacilityAtapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                facility.setAdapter(productFacilityAtapter);

                ArrayAdapter<String> productShiftAtapter = new ArrayAdapter<String>(HomeActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.productShift));
                productShiftAtapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                shift.setAdapter(productShiftAtapter);

                dateSelect = dialog.findViewById(R.id.dateSelect);
                searchDialogButton = dialog.findViewById(R.id.searchFilterButton);

                dateSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePicker = new DatePickerDialog(HomeActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        month = month +1;
                                        String dateString = year +"-"+month+"-"+dayOfMonth;
                                        dateView.setText(dayOfMonth+"."+month+"."+year);
                                        Date date = null;
                                        try {
                                            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        dateSelect.setText(new SimpleDateFormat("yyyy-MM-dd").format(date));

                                    }
                                }, year, month, day);

                        datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", datePicker);
                        datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker);
                        datePicker.show();
                    }
                });
                searchDialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String getFilterDate = dateSelect.getText().toString();
                        String facilityFilter = facility.getSelectedItem().toString();
                        String shiftfilter = shift.getSelectedItem().toString();
                            getFilterDate = getFilterDate.replace("Tarih Seçiniz","");
                            if(getFilterDate == ""){
                                dateView.setText("Tüm Zamanlar");
                            }
                            facilityFilter = facilityFilter.replace("Üretim Tesisi","");
                            facilityFilter = facilityFilter.replace("Tesis ","");
                            shiftfilter = shiftfilter.replace("Vardiya","");
                            shiftfilter = shiftfilter.replace(".Vardiya","");
                            allData(FirebaseAuth.getInstance().getCurrentUser().getUid(),facilityFilter,shiftfilter,getFilterDate);
                            dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

    }

    public void allData(String userid,String facility,String shift,String date){
        final Dialog dialogwait = new Dialog(HomeActivity.this);
        dialogwait.setContentView(R.layout.wait_dialog);
        dialogwait.show();
        dialogwait.setCanceledOnTouchOutside(false);
        dialogwait.setCancelable(false);
        productDIF.allData(userid,facility,shift,date).enqueue(new Callback<ProductDataList>() {
            @Override
            public void onResponse(Call<ProductDataList> call, Response<ProductDataList> response) {

                List<Datum> dataList = response.body().getData();
                adapter = new ProductAdapter(HomeActivity.this,dataList);
                if(dataList != null){
                    totalTag = 0;
                    totalWeightCount = 0;
                    for(Datum d: dataList){
                        totalTag += Integer.parseInt(d.getPackagePalette());
                        totalWeightCount += Integer.parseInt(d.getWeight());
                    }
                    totalTagView.setText(Integer.toString(totalTag));
                    totalWeight.setText(Integer.toString(totalWeightCount) + " KG");
                    rv.setVisibility(View.VISIBLE);
                    rv.setAdapter(adapter);
                    dialogwait.dismiss();
                }else{
                    totalTagView.setText("0");
                    totalWeight.setText("0");
                    dialogwait.dismiss();
                    rv.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ProductDataList> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int menuId = menuItem.getItemId();
        if(menuId == R.id.action_settings){
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_sales){
            Intent intent = new Intent(getApplicationContext(), SalesActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_transport){
            Intent intent = new Intent(getApplicationContext(), ShippingActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_update){
            Intent intent = new Intent(getApplicationContext(), UpdateActivity.class);
            startActivity(intent);
        }else if(menuId == R.id.action_logout){
            sp = getSharedPreferences("cuser",MODE_PRIVATE);
            editor = sp.edit();
            sp.edit().remove("cuser").commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(HomeActivity.this, "Oturumunuz kapatılmıştır.", Toast.LENGTH_LONG).show();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
