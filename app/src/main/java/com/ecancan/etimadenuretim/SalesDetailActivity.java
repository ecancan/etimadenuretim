package com.ecancan.etimadenuretim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.Datum;
import com.ecancan.etimadenuretim.Modules.SalesDatum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SalesDetailActivity extends AppCompatActivity {
    private SalesDatum salesDatum;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private TextView salesDetailContent;
    private Date dateCekme,dateYukleme,dateKantar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_detail);

        Intent i = getIntent();
        salesDatum = (SalesDatum) i.getSerializableExtra("salesDetail");

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        toolbarTitle.setText(salesDatum.getTeslimatNo()+" Detay");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            dateCekme = new SimpleDateFormat("yyyy-MM-dd").parse(salesDatum.getCekmeTarihi());
            dateYukleme = new SimpleDateFormat("yyyy-MM-dd").parse(salesDatum.getYuklemeTarihi());
            dateKantar = new SimpleDateFormat("yyyy-MM-dd").parse(salesDatum.getBirKantarIslemTarihi());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        salesDetailContent = findViewById(R.id.singleContent);

        salesDetailContent.setText(
                "Teslimat No : \n" + salesDatum.getTeslimatNo()+"\n\n" +
                        "Sefer No : \n" + salesDatum.getSeferNo()+"\n\n" +
                        "Malzeme Acıklaması : \n" + salesDatum.getMalzemeKisa()+"\n\n" +
                        "Parti : \n" + salesDatum.getParti()+"\n\n" +
                        "Teslimat Miktarı : \n" + salesDatum.getTeslimatMiktari()+"\n\n" +
                        "Teslim Eden Yer : \n" + salesDatum.getTeslimEdenYer()+"\n\n" +
                        "Depo Yeri : \n" + salesDatum.getDepoYeri()+"\n\n" +
                        "Konteyner No : \n" + salesDatum.getKonteynerNo()+"\n\n" +
                        "Booking No : \n" + salesDatum.getBookingNo()+"\n\n" +
                        "Konteyner Türü : \n" + salesDatum.getKonteynerNo()+"\n\n" +
                        "Ambalaj Açıklaması : \n" + salesDatum.getAmbalajAciklama()+"\n\n" +
                        "Palet Tip Açıklaması : \n" + salesDatum.getPaletTipAciklama()+"\n\n" +
                        "Torba Tanımı : \n" + salesDatum.getTorbaTanimi()+"\n\n" +
                        "Palet Türü : \n" + salesDatum.getPaletTAciklama()+"\n\n" +
                        "Torbalama Acıklaması : \n" + salesDatum.getTorbalamaAciklama()+"\n\n" +
                        "Shrink : \n" + salesDatum.getShrink()+"\n\n" +
                        "Çift Kulak : \n" + salesDatum.getCiftKulak()+"\n\n" +
                        "Alttan Boşaltma : \n" + salesDatum.getAlttanBosaltma()+"\n\n" +
                        "Buffle : \n" + salesDatum.getBuffle()+"\n\n" +
                        "Palet Sayısı : \n" + salesDatum.getPaletSayisi()+"\n\n" +
                        "Tane Boyutu : \n" + salesDatum.getTaneBoyutu()+"\n\n" +
                        "Kilit No : \n" + salesDatum.getKilitNo()+"\n\n" +
                        "İlk Tartım : \n" + salesDatum.getBirKantarIlkTartim()+"\n\n" +
                        "İkinci Tartım : \n" + salesDatum.getBirKantarIkinciTartim()+"\n\n" +
                        "Kantar İşlem Tarihi : \n" + new SimpleDateFormat("dd.MM.yyyy").format(dateKantar)+"\n\n" +
                        "Kantar İşlem Saati : \n" + salesDatum.getBirKantarIslemSaati()+"\n\n" +
                        "Plaka : \n" + salesDatum.getPlaka()+"\n\n" +
                        "Çekme Tarihi : \n" + new SimpleDateFormat("dd.MM.yyyy").format(dateCekme)+"\n\n" +
                        "Yükleme Tarihi : \n" + new SimpleDateFormat("dd.MM.yyyy").format(dateYukleme)+"\n\n" +
                        "Alıcı Adı : \n" + salesDatum.getAliciAdi()+"\n\n" +
                        "Sipariş No : \n" + salesDatum.getSiparisNo()+"\n\n" +
                        "Net : \n" + salesDatum.getNet()+"\n\n" +
                        "Yükleme Limanı : \n" + salesDatum.getYuklemeLimani()+"\n\n" +
                        "Boşaltma Limanı : \n" + salesDatum.getBosaltmaLimani()+"\n\n" +
                        "Zayi Edilen Miktar : \n" + salesDatum.getZayiEdilenMiktar()+"\n\n" +
                        "İptal Nedeni Tanımı : \n" + salesDatum.getIptalNedeniTanimi()+"\n\n" +
                        "Adı Soyadı : \n" + salesDatum.getAdiSoyadi()+"\n\n"

                );

    }
}
