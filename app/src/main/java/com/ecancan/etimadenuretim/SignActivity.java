package com.ecancan.etimadenuretim;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenuretim.Modules.Hash;
import com.ecancan.etimadenuretim.Modules.Settings;
import com.ecancan.etimadenuretim.Modules.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignActivity extends AppCompatActivity {
    private EditText userMailEditText;
    private EditText userPasswordEditText;
    private EditText userPasswordEditTextRepeat;
    private Button loginButton;
    private TextView loginLink;
    private String userMail;
    private String userPassword;
    private String userPasswordRepeat;
    private Users users;
    private FirebaseDatabase database;
    private DatabaseReference rootRef,rootRefSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        rootRefSettings = database.getReference("settings");

        userMailEditText = findViewById(R.id.userMailEditText);
        userPasswordEditText = findViewById(R.id.userPasswordEditText);
        userPasswordEditTextRepeat = findViewById(R.id.userPasswordEditTextRepeat);
        loginButton = findViewById(R.id.userLoginButton);
        loginLink = findViewById(R.id.loginLinkTextView);

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userMail = userMailEditText.getText().toString();
                userPassword = userPasswordEditText.getText().toString();
                userPasswordRepeat = userPasswordEditTextRepeat.getText().toString();
                if(!userMail.isEmpty() && !userPassword.isEmpty() && !userPasswordRepeat.isEmpty()){
                    if(userPassword.equals(userPasswordRepeat)){
                        if(android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches()){

                            rootRefSettings.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    Settings settings = dataSnapshot.child("userRegisterStatus").getValue(Settings.class);
                                    if(settings.getUserRegistering().equals("Aktif")){
                                        signProcess(userMail, Hash.md5(userPassword));
                                    }else{
                                        Toast.makeText(SignActivity.this, "Sistem üyelik alımına kapatılmıştır.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }else{
                            Toast.makeText(SignActivity.this, "Mail adresi hatalı veya eksik.", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(SignActivity.this, "Girmiş olduğunu şifreler birbirleri ile eşleşmemektedir.", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(SignActivity.this, "Boş alan bırakmayın.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void signProcess(final String userMail, String userPassword) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.wait_dialog);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(userMail,userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Users usersSign = new Users(FirebaseAuth.getInstance().getCurrentUser().getUid(),"Standart",userMail,"",0,"");
                    rootRef.child(usersSign.getUserKey()).setValue(usersSign, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            Intent intent = new Intent(SignActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finishAffinity();
                            dialog.dismiss();
                            Toast.makeText(SignActivity.this, "Kayıt işlemi başarılı.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    String errorCode = ((FirebaseAuthException) task.getException()).getErrorCode();

                    switch (errorCode) {

                        case "ERROR_INVALID_CUSTOM_TOKEN":
                            Toast.makeText(SignActivity.this, "Özel simge biçimi yanlış. Lütfen belgeleri kontrol edin.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_CUSTOM_TOKEN_MISMATCH":
                            Toast.makeText(SignActivity.this, "Özel belirteç farklı bir kitleye karşılık gelir.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_CREDENTIAL":
                            Toast.makeText(SignActivity.this, "Sağlanan kimlik doğrulama bilgileri hatalı biçimlendirilmiş veya süresi dolmuş.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_EMAIL":
                            Toast.makeText(SignActivity.this, "Mail adresiniz hatalı veya eksik.", Toast.LENGTH_LONG).show();
                            userMailEditText.requestFocus();
                            break;

                        //case "ERROR_WRONG_PASSWORD":
                        //    Toast.makeText(SignActivity.this, "Şifreniz hatalı.", Toast.LENGTH_LONG).show();
                        //    userPasswordEditText.requestFocus();
                        //    userPasswordEditText.setText("");
                        //    break;

                        //case "ERROR_USER_MISMATCH":
                        //    Toast.makeText(SignActivity.this, "Verilen kimlik bilgileri daha önce oturum açmış olan kullanıcıya karşılık gelmiyor.", Toast.LENGTH_LONG).show();
                        //  break;

                        case "ERROR_REQUIRES_RECENT_LOGIN":
                            Toast.makeText(SignActivity.this, "Bu işlem hassas ve son kimlik doğrulama gerektiriyor. Bu isteği yeniden denemeden önce tekrar giriş yapın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                            Toast.makeText(SignActivity.this, "Aynı e-posta adresiyle, ancak farklı oturum açma kimlik bilgileriyle bir hesap zaten var. Bu e-posta adresiyle ilişkili bir sağlayıcı kullanarak oturum açın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_EMAIL_ALREADY_IN_USE":
                            Toast.makeText(SignActivity.this, "Bu mail adresi zaten kullanılmış.", Toast.LENGTH_LONG).show();
                            userMailEditText.requestFocus();
                            break;

                        case "ERROR_CREDENTIAL_ALREADY_IN_USE":
                            Toast.makeText(SignActivity.this, "Bu kullanıcı bilgileri zaten sistemde mevcut.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_DISABLED":
                            Toast.makeText(SignActivity.this, "Bu kullanıcı yönetici tarafından yasaklanmıştır.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_TOKEN_EXPIRED":
                            Toast.makeText(SignActivity.this, "Bu kullanıcının özel kimliğinin süresi dolmuştur.", Toast.LENGTH_LONG).show();
                            break;

                        //case "ERROR_USER_NOT_FOUND":
                        //   Toast.makeText(SignActivity.this, "There is no user record corresponding to this identifier. The user may have been deleted.", Toast.LENGTH_LONG).show();
                        //    break;

                        case "ERROR_INVALID_USER_TOKEN":
                            Toast.makeText(SignActivity.this, "Geçersiz kullanıcı kimliği.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_OPERATION_NOT_ALLOWED":
                            Toast.makeText(SignActivity.this, "Bu işleme izin verilmiyor. Konsolda bu hizmeti etkinleştirmelisiniz.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_WEAK_PASSWORD":
                            Toast.makeText(SignActivity.this, "Girdiğiniz şifrenin güvenliği zayıf. Daha güçlü bir şifre kullanın.", Toast.LENGTH_LONG).show();
                            userPasswordEditText.requestFocus();
                            break;

                    }

                    //Toast.makeText(SignActivity.this, "Bir şeyler ters gitti. - "+ task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }

            }
        });
    }

}
