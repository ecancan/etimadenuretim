package com.ecancan.etimadenuretim;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.ecancan.etimadenuretim.BuildConfig;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class UpdateActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private Button updateButton;
    private TextView updateMsg;
    private ImageView updateImg;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        final int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        updateMsg = findViewById(R.id.updateMsg);
        updateButton = findViewById(R.id.updateButton);
        updateImg = findViewById(R.id.updateImg);

        updateButton.setVisibility(View.INVISIBLE);


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseRef= firebaseDatabase.getReference().child("updateVersion");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int value = dataSnapshot.getValue(Integer.class);
                if(value > versionCode){
                    updateMsg.setText("YENİ GÜNCELLEME MEVCUT!");
                    updateImg.setImageResource(R.drawable.updateerror);
                    updateButton.setVisibility(View.VISIBLE);
                    updateButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent viewIntent = new Intent("android.intent.action.VIEW",
                                    Uri.parse("http://www.ecancan.com/indir/EtimadenUretim.apk"));
                            startActivity(viewIntent);
                        }
                    });
                }else{
                    updateMsg.setText("TEBRİKLER UYGULAMANIZ \n" +
                            "GÜNCEL");
                    updateButton.setVisibility(View.INVISIBLE);
                    updateImg.setImageResource(R.drawable.updateok);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        toolbarTitle.setText("Güncelleme Kontrol");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



    }
}
