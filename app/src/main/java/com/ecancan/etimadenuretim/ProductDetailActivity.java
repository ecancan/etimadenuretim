package com.ecancan.etimadenuretim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.Datum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductDetailActivity extends AppCompatActivity {
    private Datum productDetail;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private TextView productCode,productFacilityAndShift,
            productWeight,productSize,productBaggedShape,
            productBaggedType,productPaletteType,
            productType,productTagCount,productDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Intent i = getIntent();
        productDetail = (Datum) i.getSerializableExtra("productDetail");

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        toolbarTitle.setText(productDetail.getOrders()+" Detay");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        productCode = findViewById(R.id.satinAlmaBelgesiView);
        productFacilityAndShift = findViewById(R.id.productFacility);
        productWeight = findViewById(R.id.productWeight);
        productSize = findViewById(R.id.productSize);
        productBaggedShape = findViewById(R.id.productBaggedShape);
        productBaggedType = findViewById(R.id.productBaggedType);
        productPaletteType = findViewById(R.id.productPaletteType);
        productType = findViewById(R.id.productType);
        productTagCount = findViewById(R.id.productTagCount);
        productDate = findViewById(R.id.productDate);

        productCode.setText(productDetail.getOrders());
        productFacilityAndShift.setText("Tesis "+productDetail.getFacility()+" - V"+ productDetail.getShift());
        productWeight.setText(productDetail.getWeight() + " KG");
        productSize.setText(productDetail.getSize());
        productBaggedShape.setText(productDetail.getBaggedShape());
        productBaggedType.setText("Ç: "+productDetail.getC()+" A: "+productDetail.getA()+" B: "+productDetail.getB()+" C: "+productDetail.getC2()+" K: "+productDetail.getK());
        productPaletteType.setText(productDetail.getBagType());
        productType.setText(productDetail.getPackageType());
        productTagCount.setText(productDetail.getPackagePalette() + "ETK");
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(productDetail.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        productDate.setText(new SimpleDateFormat("dd.MM.yyyy").format(date));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
