package com.ecancan.etimadenuretim;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ecancan.etimadenuretim.Modules.FirebaseAuthLoginInfo;
import com.ecancan.etimadenuretim.Modules.Hash;
import com.ecancan.etimadenuretim.Modules.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    private EditText userMailEditText;
    private EditText userPasswordEditText;
    private Button loginButton;
    private TextView registerLink;
    private String userMail;
    private String userPassword;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private FirebaseAuthLoginInfo FbLoginInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");
        sp = getSharedPreferences("cuser",MODE_PRIVATE);
        editor = sp.edit();
        userMailEditText = findViewById(R.id.userMailEditText);
        userPasswordEditText = findViewById(R.id.userPasswordEditText);
        loginButton = findViewById(R.id.userLoginButton);
        registerLink = findViewById(R.id.registerLinkTextView);




        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userMail = userMailEditText.getText().toString();
                userPassword = userPasswordEditText.getText().toString();
                if(!userMail.isEmpty() && !userPassword.isEmpty()){
                    if(android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches()){
                        loginProcess(userMail, Hash.md5(userPassword));
                    }else{
                        Toast.makeText(MainActivity.this, "Mail adresi hatalı veya eksik.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Boş alan bırakmayın.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void loginProcess(final String userMail, final String userPassword) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.wait_dialog);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        FirebaseAuth.getInstance().signInWithEmailAndPassword(userMail,userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FbLoginInfo = new FirebaseAuthLoginInfo(userMail,userPassword);
                    Gson gson = new Gson();
                    String json = gson.toJson(FbLoginInfo);
                    editor.putString("cuser", json);
                    editor.commit();
                    //Users userslogin = new Users();
                    //userslogin.setUserToken();
                    dialog.dismiss();
                    Toast.makeText(MainActivity.this, "Giriş işlemi başarılı.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finishAffinity();
                }else{
                    String errorCode = ((FirebaseAuthException) task.getException()).getErrorCode();

                    switch (errorCode) {

                        case "ERROR_INVALID_CUSTOM_TOKEN":
                            Toast.makeText(MainActivity.this, "Özel simge biçimi yanlış. Lütfen belgeleri kontrol edin.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_CUSTOM_TOKEN_MISMATCH":
                            Toast.makeText(MainActivity.this, "Özel belirteç farklı bir kitleye karşılık gelir.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_CREDENTIAL":
                            Toast.makeText(MainActivity.this, "Sağlanan kimlik doğrulama bilgileri hatalı biçimlendirilmiş veya süresi dolmuş.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_EMAIL":
                            Toast.makeText(MainActivity.this, "Mail adresiniz hatalı veya eksik.", Toast.LENGTH_LONG).show();
                            userMailEditText.requestFocus();
                            break;

                        case "ERROR_WRONG_PASSWORD":
                            Toast.makeText(MainActivity.this, "Şifreniz hatalı.", Toast.LENGTH_LONG).show();
                            userPasswordEditText.requestFocus();
                            userPasswordEditText.setText("");
                            break;

                        case "ERROR_USER_MISMATCH":
                            Toast.makeText(MainActivity.this, "Verilen kimlik bilgileri daha önce oturum açmış olan kullanıcıya karşılık gelmiyor.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_REQUIRES_RECENT_LOGIN":
                            Toast.makeText(MainActivity.this, "Bu işlem hassas ve son kimlik doğrulama gerektiriyor. Bu isteği yeniden denemeden önce tekrar giriş yapın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                            Toast.makeText(MainActivity.this, "Aynı e-posta adresiyle, ancak farklı oturum açma kimlik bilgileriyle bir hesap zaten var. Bu e-posta adresiyle ilişkili bir sağlayıcı kullanarak oturum açın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_EMAIL_ALREADY_IN_USE":
                            Toast.makeText(MainActivity.this, "Bu mail adresi zaten kullanılmış.", Toast.LENGTH_LONG).show();
                            userMailEditText.requestFocus();
                            break;

                        case "ERROR_CREDENTIAL_ALREADY_IN_USE":
                            Toast.makeText(MainActivity.this, "Bu kullanıcı bilgileri zaten sistemde mevcut.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_DISABLED":
                            Toast.makeText(MainActivity.this, "Bu kullanıcı yönetici tarafından yasaklanmıştır.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_TOKEN_EXPIRED":
                            Toast.makeText(MainActivity.this, "Bu kullanıcının özel kimliğinin süresi dolmuştur.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_NOT_FOUND":
                           Toast.makeText(MainActivity.this, "There is no user record corresponding to this identifier. The user may have been deleted.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_USER_TOKEN":
                            Toast.makeText(MainActivity.this, "Geçersiz kullanıcı kimliği.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_OPERATION_NOT_ALLOWED":
                            Toast.makeText(MainActivity.this, "Bu işleme izin verilmiyor. Konsolda bu hizmeti etkinleştirmelisiniz.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_WEAK_PASSWORD":
                            Toast.makeText(MainActivity.this, "Girdiğiniz şifrenin güvenliği zayıf. Daha güçlü bir şifre kullanın.", Toast.LENGTH_LONG).show();
                            userPasswordEditText.requestFocus();
                            break;

                    }

                    //Toast.makeText(SignActivity.this, "Bir şeyler ters gitti. - "+ task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }
        });


    }


}
