package com.ecancan.etimadenuretim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Modules.SalesDatum;
import com.ecancan.etimadenuretim.Modules.ShippingDatum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShippingDetailActivity extends AppCompatActivity {
    private ShippingDatum shippingDatum;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private TextView shippingDetailContent;
    private Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_detail);
        Intent i = getIntent();
        shippingDatum = (ShippingDatum) i.getSerializableExtra("shippingDetail");

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        toolbarTitle.setText(shippingDatum.getSatinAlmaBelgesi()+" Detay");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(shippingDatum.getIslemTarihi());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        shippingDetailContent = findViewById(R.id.singleContent);

        shippingDetailContent.setText(
                "Teslimat No : \n" + shippingDatum.getTeslimatNo()+"\n\n" +
                        "Sefer No : \n" + shippingDatum.getSeferNo()+"\n\n" +
                        "Satın Alma Belgesi : \n" + shippingDatum.getSatinAlmaBelgesi()+"\n\n" +
                        "Malzeme No : \n" + shippingDatum.getMalzeme()+"\n\n" +
                        "Malmzeme Kısa Metni : \n" + shippingDatum.getMalzemeKisaMetni()+"\n\n" +
                        "Parti : \n" + shippingDatum.getParti()+"\n\n" +
                        "Teslim Eden Üretim Adı : \n" + shippingDatum.getTeslimEdenUretimAdi()+"\n\n" +
                        "Teslim Eden Depo Adı : \n" + shippingDatum.getTeslimEdenDepoAdi()+"\n\n" +
                        "Tane Boyutu : \n" + shippingDatum.getTaneBoyutu()+"\n\n" +
                        "Ölçülen Miktar : \n" + shippingDatum.getOlculenMiktar()+"\n\n" +
                        "İlk Tartım : \n" + shippingDatum.getIlkTartim()+"\n\n" +
                        "İkinci Tartım : \n" + shippingDatum.getIkinciTartim()+"\n\n" +
                        "İşlem Tarihi : \n" + shippingDatum.getIslemTarihi()+"\n\n" +
                        "İşlem Saati : \n" + shippingDatum.getIslemSaati()+"\n\n" +
                        "Plaka : \n" + shippingDatum.getPlaka()+"\n\n" +
                        "Teslim Alan Depo Yeri : \n" + shippingDatum.getTeslimAlanDepoYeri()+"\n\n" +
                        "Birinci Kantar No : \n" + shippingDatum.getBirKantarNo()+"\n\n" +
                        "Birinci Kantar : \n" + shippingDatum.getBirinciKantar()+"\n\n" +
                        "İkinci Kantar No : \n" + shippingDatum.getIkiKantarNo()+"\n\n" +
                        "İkinci Kantar : \n" + shippingDatum.getIkinciKantar()+"\n\n" +
                        "Sofor Adı Soyadı : \n" + shippingDatum.getSoforAdiSoyadi()+"\n\n"
        );

    }
}
