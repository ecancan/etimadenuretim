package com.ecancan.etimadenuretim;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecancan.etimadenuretim.Adapters.ShippingAdapter;
import com.ecancan.etimadenuretim.Interfaces.IProductDataAccessObject;
import com.ecancan.etimadenuretim.Modules.ApiUtils;
import com.ecancan.etimadenuretim.Modules.ShippingDataList;
import com.ecancan.etimadenuretim.Modules.ShippingDatum;
import com.ecancan.etimadenuretim.Modules.ShippingSearchFilter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShippingActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private IProductDataAccessObject shippingDIF;
    private ShippingAdapter adapter;
    private RecyclerView rv;
    private int totalVehicleCount = 0;
    private ImageView searchButton;
    private EditText satinAlmaBelgesiNo,malzemeNo,birKantarNo,ikiKantarNo,plakaNo;
    private Button searchDialogButton,searchDialogButtonReset;
    private TextView dateSelect;
    private TextView dateView;
    private TextView totalVehicle,totalSalesWeightView;
    private int totalSalesWeightCount = 0;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private ShippingSearchFilter shippingSearchFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);
        searchButton = findViewById(R.id.searchButton);
        dateView = findViewById(R.id.dateView);
        totalVehicle = findViewById(R.id.totalVehicle);
        totalSalesWeightView = findViewById(R.id.totalWeightSales);
        toolbar = findViewById(R.id.toolbar);

        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference("users");

        sp = getSharedPreferences("searchShippingFilterData",MODE_PRIVATE);
        editor = sp.edit();

        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Nakliye Listesi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        shippingDIF = ApiUtils.getIProductDataAccessObject();
        allShippingData(FirebaseAuth.getInstance().getCurrentUser().getUid(),"","","","","","");



        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ShippingActivity.this);
                dialog.setContentView(R.layout.shipping_search_dialog);
                dialog.setTitle("Nakliyat Filtreleme");
                satinAlmaBelgesiNo = dialog.findViewById(R.id.satin_alma_belgesi);
                malzemeNo = dialog.findViewById(R.id.teslimat_no);
                birKantarNo = dialog.findViewById(R.id.bir_kantar_no);
                ikiKantarNo = dialog.findViewById(R.id.iki_kantar_no);
                plakaNo = dialog.findViewById(R.id.plaka);

                dateSelect = dialog.findViewById(R.id.dateSelect);
                searchDialogButton = dialog.findViewById(R.id.searchFilterButton);
                searchDialogButtonReset = dialog.findViewById(R.id.searchFilterButtonReset);

                try{
                    sp = getSharedPreferences("searchShippingFilterData",MODE_PRIVATE);
                    editor = sp.edit();
                    Gson gson = new Gson();
                    String json = sp.getString("searchShippingFilterData", "");
                    shippingSearchFilter = gson.fromJson(json, ShippingSearchFilter.class);
                    dateSelect.setText(shippingSearchFilter.getIslem_tarihi());
                    satinAlmaBelgesiNo.setText(shippingSearchFilter.getSatin_alma_belgesi());
                    malzemeNo.setText(shippingSearchFilter.getMalzeme());
                    birKantarNo.setText(shippingSearchFilter.getBir_kantar_no());
                    ikiKantarNo.setText(shippingSearchFilter.getIki_kantar_no());
                    plakaNo.setText(shippingSearchFilter.getPlaka());
                }catch (Exception e){

                }

                dateSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int day = calendar.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePicker = new DatePickerDialog(ShippingActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        month = month +1;
                                        String dateString = year +"-"+month+"-"+dayOfMonth;
                                        dateView.setText(dayOfMonth+"."+month+"."+year);
                                        Date date = null;
                                        try {
                                            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        dateSelect.setText(new SimpleDateFormat("yyyy-MM-dd").format(date));

                                    }
                                }, year, month, day);

                        datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", datePicker);
                        datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker);
                        datePicker.show();
                    }
                });
                searchDialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String getFilterDate = dateSelect.getText().toString();
                        String satinAlmaBelgesi = satinAlmaBelgesiNo.getText().toString();
                        String malzeme = malzemeNo.getText().toString();
                        String birKantar = birKantarNo.getText().toString();
                        String ikiKantar = ikiKantarNo.getText().toString();
                        String plaka = plakaNo.getText().toString();

                        shippingSearchFilter = new ShippingSearchFilter(satinAlmaBelgesi,malzeme,birKantar,ikiKantar,getFilterDate,plaka);
                        Gson gson = new Gson();
                        String json = gson.toJson(shippingSearchFilter);
                        editor.putString("searchShippingFilterData", json);
                        editor.commit();

                        getFilterDate = getFilterDate.replace("Tarih Seçiniz","");
                        Log.e("date",getFilterDate);
                        Log.e("satinAlma",satinAlmaBelgesi);
                        Log.e("malzeme",malzeme);
                        Log.e("birkantar",birKantar);
                        Log.e("ikikantar",ikiKantar);
                        if(getFilterDate == ""){
                            dateView.setText("Tüm Zamanlar");
                        }


                        allShippingData(FirebaseAuth.getInstance().getCurrentUser().getUid(),satinAlmaBelgesi,malzeme,birKantar,ikiKantar,getFilterDate,plaka);
                        dialog.dismiss();
                    }
                });
                searchDialogButtonReset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateSelect.setText("Tarih Seçiniz");
                        satinAlmaBelgesiNo.setText("");
                        malzemeNo.setText("");
                        birKantarNo.setText("");
                        ikiKantarNo.setText("");
                        sp = getSharedPreferences("searchShippingFilterData",MODE_PRIVATE);
                        editor = sp.edit();
                        sp.edit().remove("searchShippingFilterData").commit();
                    }
                });

                dialog.show();
            }
        });

    }

    public void allShippingData(String userid,String satin_alma_belgesi,String malzeme,String bir_kantar_no,String iki_kantar_no,String islem_tarihi,String plaka){
        final Dialog dialogwait = new Dialog(ShippingActivity.this);
        dialogwait.setContentView(R.layout.wait_dialog);
        dialogwait.show();
        dialogwait.setCanceledOnTouchOutside(false);
        dialogwait.setCancelable(false);
        shippingDIF.allShippingData(userid,satin_alma_belgesi,malzeme,bir_kantar_no,iki_kantar_no,islem_tarihi,plaka).enqueue(new Callback<ShippingDataList>() {
            @Override
            public void onResponse(Call<ShippingDataList> call, Response<ShippingDataList> response) {

                List<ShippingDatum> dataList = response.body().getData();
                adapter = new ShippingAdapter(ShippingActivity.this,dataList);
                if(dataList != null){
                    totalVehicleCount = 0;
                    totalSalesWeightCount = 0;
                    for(ShippingDatum d: dataList){
                        totalVehicleCount++;
                        totalSalesWeightCount += Integer.parseInt(d.getOlculenMiktar());
                    }
                    totalVehicle.setText(Integer.toString(totalVehicleCount));
                    totalSalesWeightView.setText(Integer.toString(totalSalesWeightCount) +" KG");
                    rv.setVisibility(View.VISIBLE);
                    rv.setAdapter(adapter);
                    dialogwait.dismiss();
                }else{
                    totalVehicle.setText("0");
                    totalSalesWeightView.setText("0 KG");
                    dialogwait.dismiss();
                    rv.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ShippingDataList> call, Throwable t) {

            }
        });
    }
}
