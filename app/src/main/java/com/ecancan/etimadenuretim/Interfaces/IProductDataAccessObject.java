package com.ecancan.etimadenuretim.Interfaces;
import com.ecancan.etimadenuretim.Modules.SalesDataList;
import com.ecancan.etimadenuretim.Modules.ProductDataList;
import com.ecancan.etimadenuretim.Modules.ShippingDataList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IProductDataAccessObject {
    @POST("listjson")
    @FormUrlEncoded
    Call<ProductDataList> allData(@Field("userid") String userid, @Field("facility") String facility,@Field("shift") String shift,@Field("date") String date);

    @POST("listjsonsales")
    @FormUrlEncoded
    Call<SalesDataList> allSalesData(@Field("userid") String userid, @Field("teslimat_no") String teslimat_no,@Field("plaka") String plaka,@Field("parti") String parti,@Field("cekme_tarihi") String cekme_tarihi);

    @POST("listjsonshipping")
    @FormUrlEncoded
    Call<ShippingDataList> allShippingData(@Field("userid") String userid,@Field("satin_alma_belgesi") String satin_alma_belgesi,@Field("malzeme") String malzeme,@Field("bir_kantar_no") String bir_kantar_no,@Field("iki_kantar_no") String iki_kantar_no,@Field("islem_tarihi") String islem_tarihi,@Field("plaka") String plaka);
}
