package com.ecancan.etimadenuretim;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.ecancan.etimadenuretim.Modules.FirebaseAuthLoginInfo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.gson.Gson;

public class SplashScreen extends AppCompatActivity {
    private FirebaseAuthLoginInfo FbLoginInfo;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        try{
            
            sp = getSharedPreferences("cuser",MODE_PRIVATE);
            editor = sp.edit();
            Gson gson = new Gson();
            String json = sp.getString("cuser", "");
            FbLoginInfo = gson.fromJson(json, FirebaseAuthLoginInfo.class);
            if(!FbLoginInfo.getUserMail().isEmpty() && !FbLoginInfo.getPassword().isEmpty()){
                loginProcess(FbLoginInfo.getUserMail(), FbLoginInfo.getPassword());
            }else{
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finishAffinity();
            }
        }catch (Exception e){
            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
        }
    }
    private void loginProcess(final String userMail, final String userPassword) {
        /*
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.wait_dialog);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        */


        FirebaseAuth.getInstance().signInWithEmailAndPassword(userMail,userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    //Users userslogin = new Users();
                    //userslogin.setUserToken();
                    //dialog.dismiss();
                    Toast.makeText(SplashScreen.this, "Giriş işlemi başarılı.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finishAffinity();
                }else{
                    String errorCode = ((FirebaseAuthException) task.getException()).getErrorCode();

                    switch (errorCode) {

                        case "ERROR_INVALID_CUSTOM_TOKEN":
                            Toast.makeText(SplashScreen.this, "Özel simge biçimi yanlış. Lütfen belgeleri kontrol edin.", Toast.LENGTH_LONG).show();

                            break;

                        case "ERROR_CUSTOM_TOKEN_MISMATCH":
                            Toast.makeText(SplashScreen.this, "Özel belirteç farklı bir kitleye karşılık gelir.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_CREDENTIAL":
                            Toast.makeText(SplashScreen.this, "Sağlanan kimlik doğrulama bilgileri hatalı biçimlendirilmiş veya süresi dolmuş.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_EMAIL":
                            Toast.makeText(SplashScreen.this, "Mail adresiniz hatalı veya eksik.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_WRONG_PASSWORD":
                            Toast.makeText(SplashScreen.this, "Şifreniz hatalı.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_MISMATCH":
                            Toast.makeText(SplashScreen.this, "Verilen kimlik bilgileri daha önce oturum açmış olan kullanıcıya karşılık gelmiyor.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_REQUIRES_RECENT_LOGIN":
                            Toast.makeText(SplashScreen.this, "Bu işlem hassas ve son kimlik doğrulama gerektiriyor. Bu isteği yeniden denemeden önce tekrar giriş yapın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                            Toast.makeText(SplashScreen.this, "Aynı e-posta adresiyle, ancak farklı oturum açma kimlik bilgileriyle bir hesap zaten var. Bu e-posta adresiyle ilişkili bir sağlayıcı kullanarak oturum açın.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_EMAIL_ALREADY_IN_USE":
                            Toast.makeText(SplashScreen.this, "Bu mail adresi zaten kullanılmış.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_CREDENTIAL_ALREADY_IN_USE":
                            Toast.makeText(SplashScreen.this, "Bu kullanıcı bilgileri zaten sistemde mevcut.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_DISABLED":
                            Toast.makeText(SplashScreen.this, "Bu kullanıcı yönetici tarafından yasaklanmıştır.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_TOKEN_EXPIRED":
                            Toast.makeText(SplashScreen.this, "Bu kullanıcının özel kimliğinin süresi dolmuştur.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_USER_NOT_FOUND":
                            Toast.makeText(SplashScreen.this, "There is no user record corresponding to this identifier. The user may have been deleted.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_INVALID_USER_TOKEN":
                            Toast.makeText(SplashScreen.this, "Geçersiz kullanıcı kimliği.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_OPERATION_NOT_ALLOWED":
                            Toast.makeText(SplashScreen.this, "Bu işleme izin verilmiyor. Konsolda bu hizmeti etkinleştirmelisiniz.", Toast.LENGTH_LONG).show();
                            break;

                        case "ERROR_WEAK_PASSWORD":
                            Toast.makeText(SplashScreen.this, "Girdiğiniz şifrenin güvenliği zayıf. Daha güçlü bir şifre kullanın.", Toast.LENGTH_LONG).show();
                            break;

                    }
                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finishAffinity();
                    //Toast.makeText(SignActivity.this, "Bir şeyler ters gitti. - "+ task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    //dialog.dismiss();
                }
            }
        });


    }
}
